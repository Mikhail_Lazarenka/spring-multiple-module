package ml.web;

import ml.domain.Hotel;
import ml.repository.HotelRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class HotelController {
    private HotelRepository hotelReposotory;

    public HotelController(HotelRepository hotelReposotory) {
        this.hotelReposotory = hotelReposotory;
    }

    @GetMapping("/hotels")
    public List<Hotel> getHotels(){
        List<Hotel> list = this.hotelReposotory.findAll();
        return list;
    }
}
